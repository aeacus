<?php
$pages = array( array( "issues",     "issue_list"     ),
                array( "milestones", "milestone_list" ),
                array( "new",        "new_issue"      ) );
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<title><?php echo $cont_title; ?></title>
<link rel="stylesheet" type="text/css" href="frontend/gitweb.css"/>
</head>
<body>
<div class="page_header"><?php echo $cont_title; ?></div>
<div class="page_nav">
<?php
foreach($pages as $page) {
    if ($_GET['id'] == $page[1])
        echo $page[0].' | ';
    else
        echo '<a href="?id='.$page[1].'">'.$page[0].'</a> | ';
}
?>
<br />
