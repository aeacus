<?php

function list_actions($nr, $type) {
if ($type=='new' || $type=='reopen')
    echo '<a href="?id=action&nr='.$nr.'&action=take">take</a> | <a href="#">dispose</a> | <a href="#">assign</a>';
else if ($type=='assigned')
    echo '<a href="?id=action&nr='.$nr.'&action=take">take</a> | <a href="?id=action&nr='.$nr.'&action=fixed">fixed</a>';
else if ($type=='fixed')
    echo '<a href="?id=action&nr='.$nr.'&action=checked">checked</a> | <a title="in format: tar.gz" href="?id=action&nr='.$nr.'&action=snapshot">snapshot</a>';
else if ($type=='verifed' || $type=='closed')
    echo '<a href="?id=action&nr='.$nr.'&action=reopen">reopen</a>';
echo ' | <a href="#">edit</a> | <a href="?id=action&nr='.$nr.'&action=commit">commit</a>';

}

function list_mile_actions($nr, $type) {
    if ($type!='historical')
        echo '<a href="?id=mile_action&nr='.$nr.'&action=edit">edit</a> | ';
    if ($type=='rc' || $type=='historical')
        echo '<a href="?id=mile_action&nr='.$nr.'&action=tag">tag</a> | ';
    if ($type=='rc')
        echo '<a href="?id=mile_action&nr='.$nr.'&action=release">release</a> | ';
    if ($type=='historical' || $type=='historical')
        echo '<a href="?id=mile_action&nr='.$nr.'&action=snapshot">snapshot</a> | ';
}

function compare_issue_date($a,$b) {
    list($year, $month, $day,$hour,$min,$sec) = sscanf($a[1], "%d-%d-%d %d:%d:%d");
    $a_time = mktime($hour,$min,$sec,$month,$day,$year);
    list($year, $month, $day,$hour,$min,$sec) = sscanf($b[1], "%d-%d-%d %d:%d:%d");
    $b_time = mktime($hour,$min,$sec,$month,$day,$year);
    return $b_time - $a_time;
}

function ago($time) {
  //  echo $time;
    list($year, $month, $day,$hour,$min,$sec) = sscanf($time, "%d-%d-%d %d:%d:%d");
    $time = mktime($hour,$min,$sec,$month,$day,$year);
    //echo strftime('%a, %d %b %Y %H:%M:%S %Z', $time); 
    $diff = time() - $time;
//    echo $diff;
    if ($diff < 60)
        return $diff.' sec ago';
    else if ($diff >= 60 && $diff < 3600)
        return intval($diff/60).' min ago';
    else if ($diff >= 3600 && $diff < 3600 * 24)
        return intval($diff/3600).' hours ago';
    else if ($diff >= 3600 * 24 && $diff < 3600 * 24 * 7)
        return intval($diff/(3600*24)).' days ago';
    else if ($diff >= 3600 * 24 * 7 && $diff < 3600 * 24 * 30)
        return intval($diff/(3600*24*7)).' weeks ago';
    else if ($diff >= 3600 * 24 * 30 && $diff < 3600 * 24 * 365)
        return intval($diff/(3600*24*30)).' months ago';
    else
        return intval($diff/(3600*24*365)).' years ago';
}

function user_name($id) {
    $sql = 'SELECT `name` FROM `aec_users` WHERE `id` = '.$id.'; ';
    $result = mysql_query($sql);
    $row = mysql_fetch_array($result);
    return $row[0];
}

function module_name() {
}

$type_name = array('bug', 'feature', 'improvement', 'task');

?>
