<?php
include('config.php');

mysql_connect($db_server, $db_user, $db_pass);
mysql_select_db($db_name);

include('backend/functions.php');

if (!isset($_GET['id']))
    $_GET['id']='list';

switch ($_GET['id']) {
    case 'issue_list'  : { include('backend/issue_list.php'); break; }
    case 'issue'       : { include('backend/issue.php'); break; }
    case 'milestone_list'  : { include('backend/milestone_list.php'); break; }
    case 'milestone'       : { include('backend/milestone.php'); break; }
    case 'action'      : { include('backend/action.php'); break; }
}

include('frontend/header.php');
switch ($_GET['id']) {
    case 'issue_list'  : { include('frontend/issue_list.php'); break; }
    case 'issue' : { include('frontend/issue.php'); break; }
    case 'milestone_list'  : { include('frontend/milestone_list.php'); break; }
    case 'milestone' : { include('frontend/milestone.php'); break; }
}
include('frontend/footer.php');
?>
